import 'package:tenizo/func/game_setting_update_functions.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Return the correct date format", () {
    var date1 = new DateTime(2015, 01, 21);
    var date2 = new DateTime(2015, 02, 21);
    var date3 = new DateTime(2015, 03, 21);
    var date4 = new DateTime(2015, 04, 21);
    var date5 = new DateTime(2015, 05, 21);
    var date6 = new DateTime(2015, 06, 21);
    var date7 = new DateTime(2015, 07, 21);
    var date8 = new DateTime(2015, 08, 21);
    var date9 = new DateTime(2015, 09, 21);
    var date10 = new DateTime(2015, 10, 21);
    var date11 = new DateTime(2015, 11, 21);
    var date12 = new DateTime(2015, 12, 21);

    expect(GameSettingUpdate.dateFormatter(date1), "Jan 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date2), "Feb 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date3), "Mar 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date4), "Apr 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date5), "May 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date6), "Jun 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date7), "Jul 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date8), "Aug 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date9), "Sept 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date10), "Oct 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date11), "Nov 21 , 2015");
    expect(GameSettingUpdate.dateFormatter(date12), "Dec 21 , 2015");
  });

  test("Return the correct time format", () {
    var date1 = new DateTime(2015, 12, 21, 7, 5, 2);
    var date2 = new DateTime(2015, 12, 21, 10, 10, 2);
    var date3 = new DateTime(2015, 12, 21, 04, 13, 12);
    expect(GameSettingUpdate.timeFormatter(date1), "07 : 05 : 02");
    expect(GameSettingUpdate.timeFormatter(date2), "10 : 10 : 02");
    expect(GameSettingUpdate.timeFormatter(date3), "04 : 13 : 12");
  });

  test("Return the correct time format for database", () {
    var date1 = new DateTime(2015, 12, 21, 7, 5, 2);
    var date2 = new DateTime(2015, 12, 21, 10, 10, 2);
    var date3 = new DateTime(2015, 12, 21, 04, 13, 12);
    expect(GameSettingUpdate.dbtimeFormatter(date1), "07:05:02");
    expect(GameSettingUpdate.dbtimeFormatter(date2), "10:10:02");
    expect(GameSettingUpdate.dbtimeFormatter(date3), "04:13:12");
  });
}
