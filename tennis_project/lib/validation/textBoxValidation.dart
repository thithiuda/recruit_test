class TextBoxValidation {
  static isPasswordValidateNew(value) {
    var result = {};

    String p = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~])';
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.passwordValidate;
    }
    return result;
  }

  static isEmpty(value) {
    var result = {};
    if (!['', null].contains(value)) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.empty;
    }
    return result;
  }

  static isLengthMatch(value, number) {
    var result = {};

    if (value.length >= number) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.passwordLengthMatch;
    }
    return result;
  }

  static isOnlyCharacters(value) {
    var result = {};

    String p = r'(^[0][1-9]+)|([1-9]\d*)|[$-/:-?{-~@&!"^_`\[\]]';

    RegExp regExp = new RegExp(p);
    if (!regExp.hasMatch(value)) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.notOnlyCharacter;
    }
    return result;
  }

  static isPasswordMatch(text1, text2) {
    var result = {};

    if (text1 == text2) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }
    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.passwordNotMatch;
    }
    return result;
  }

  static isInteger(value) {
    var result = {};
    try {
      int num = int.parse(value);
      if (num != 0) {
        result['state'] = true;
      } else {
        result['state'] = false;
      }
    } catch (e) {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.notInteger;
    }
    return result;
  }

  static isUrlmatch(value) {
    var result = {};

    var urlPattern =
        r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:??,.;]*)?";

    var match = new RegExp(urlPattern, caseSensitive: false);

    if (match.hasMatch(value)) {
      result['state'] = true;
    } else {
      result['state'] = false;
    }

    if (!result['state']) {
      result['errorMessage'] = ErrorMessage.urlError;
    }

    return result;
  }

  static isDivisibleByTwo(value) {
    var result = {};
    try {
      int num = int.parse(value);
      if (num != 0) {
        if (num % 2 == 0) {
          result['state'] = true;
        } else {
          result['state'] = false;
          result['errorMessage'] = ErrorMessage.notAEvenNumber;
        }
      } else {
        result['state'] = false;
        result['errorMessage'] = ErrorMessage.notInteger;
      }
    } catch (e) {
      result['state'] = false;
      result['errorMessage'] = ErrorMessage.wrongInput;
    }
    return result;
  }
}

class ErrorMessage {
  //Textbox Error Message
  static String empty = "This field is Required!";
  static String notInteger = "Enter a valid number!";
  static String notEmail = "Enter a valid email!";
  static String notDouble = "Enter a valid decimal number!";
  static String notOnlyCharacter = "Enter a valid text!";

  static String passwordNotMatch = "Passwords doesn't match";
  static String passwordValidate =
      "Must include atleast one A-Z, a-z, 0-9 & Symbol";
  static String passwordLengthMatch =
      "Password should be more than 6 characters";

  //Selectbox Error Message
  static String selectedNone = "Select an option";

  static String routeError = "Oops.. Something went wrong!";
  static String urlError = "Enter valid URL";
  static String notAEvenNumber = "Enter an even number";
  static String wrongInput = "Wrong Input";
}
