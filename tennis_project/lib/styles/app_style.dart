import 'package:flutter/material.dart';

class AppStyle {
  //App Colors
  static const color_Head = const Color(0xFF00D9D9);
  static const color_TextGreen = const Color(0xFF008585);
  static const color_Main_Brown = const Color(0xFFFBE4BF);
  static const color_Txt_Input_Border = const Color(0xFF808080);
  static const color_Bg = const Color(0xFFecf0f1);
  static const color_AppBar = const Color(0xFF00D9D9);
  static const color_Primary = const Color(0xFF0097e6);
  static const color_Success = const Color(0xFF4cd137);
  static const color_Info = const Color(0xFF00a8ff);
  static const color_Warning = const Color(0xFFfbc531);
  static const color_Error = const Color(0xFFe84118);
  static const color_dropBox = const Color(0xFFE5F9E0);
  static const color_boderBox = const Color(0xFFFBE4BF);



  //Bacis Colors of the Tennizo app
  static const primary_color = const Color(0xFF00D9D9);
  static const secondary_color = const Color(0xFFE5F9E0);
  static const ternary_color = const Color(0xFFFBE4BF);
  static const white = const Color(0xFFFFFFFF);
  static const black = const Color(0xFF000000);

  static const xxxlarge_font = 30.0;
  static const xxlarge_font = 26.0;
  static const xlarge_font = 24.0;
  static const large_font = 20.0;
  static const normal_font = 18.0;
  static const small_font = 16.0;
  static const tiny_font = 14.0;
}
