import 'dart:io';
import 'dart:ui';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../tennizo_controller_functions.dart';

class StadiumView extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _StadiumViewState createState() => _StadiumViewState();
  StadiumView({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _StadiumViewState extends State<StadiumView> with BaseControllerListner {
  BaseController controller;

  Future<File> imageFile;
  bool _imageAvailability;

  bool _mapDisplay = false;
  GoogleMapController mapController;
  LatLng _mapLocation = LatLng(0, 0);
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  static var stdName = "";
  static var stadiumId = "";
  static var stadiumAddress = "";
  static var stadiumConNo = "";
  static var stadiumNoCouet = "";
  static var stadiumResURL = "";
  static var stadiumImg;
  double stadiumLat = 0;
  double stadiumLon = 0;

  customFontStyle() {
    return TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  customFont(text) {
    return Text(text, textAlign: TextAlign.left, style: customFontStyle(),
      overflow: TextOverflow.ellipsis,);
  }

  @override
  void initState() {
    super.initState();

    if (widget.arguments['stadiumId'].toString() != "") {
      setState(() {
        stadiumId = widget.arguments['stadiumId'].toString();
        stdName = widget.arguments['stadiumName'].toString();
        stadiumAddress = widget.arguments['stadiumAddress'].toString();
        stadiumConNo = widget.arguments['contactNumber'].toString();
        stadiumNoCouet = widget.arguments['noOfCourts'].toString();
        stadiumResURL = widget.arguments['reservationUrl'].toString();
        stadiumImg = widget.arguments['stadiumImage'];
        stadiumLat = widget.arguments['stadiumLat'];
        stadiumLon = widget.arguments['stadiumLon'];
      });

      var contactNo = stadiumConNo.split('-');
      stadiumConNo = contactNo[0] + contactNo[1];
    }
    controller = new BaseController(this);

    checkImage(stadiumImg);
    checkLocation(stadiumLat, stadiumLon);
  }

  //Check image availaibilty
  void checkImage(stadiumImg) {
    if (stadiumImg != null) {
      setState(() {
        _imageAvailability = checkImageAvailability(File(stadiumImg));
      });
    } else {
      setState(() {
        _imageAvailability = false;
      });
    }
  }

  //check map location is set
  void checkLocation(stadiumLat, stadiumLon) {
    if (stadiumLat != 0 && stadiumLon != 0) {
      setState(() {
        _mapLocation = LatLng(stadiumLat, stadiumLon);
        _mapDisplay = true;
      });
    } else {
      setState(() {
        _mapDisplay = false;
      });
    }
  }

  //loading map
  void _onMapCreated(GoogleMapController mcontroller) {
    mapController = mcontroller;
    markers.clear();
    _addMarker();
  }

  //adding maker to map
  void _addMarker() {
    openMap(LatLng _loc) => {
          controller.execFunction(
              ControllerFunc.launch_app,
              ControllerSubFunc.launch_map,
              {'lat': _loc.latitude, 'long': _loc.longitude}),
        };
    final String _markerIdVal = 'marker';
    final MarkerId _markerId = MarkerId(_markerIdVal);

    final Marker marker = Marker(
        markerId: _markerId,
        position: _mapLocation,
        consumeTapEvents: true,
        onTap: () => openMap(_mapLocation),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

    setState(() {
      markers[_markerId] = marker;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: Container(
                              width: double.infinity,
                              height: 200,
                              decoration: BoxDecoration(
                                color: AppColors.gray,
                                image: _imageAvailability
                                    ? DecorationImage(
                                        fit: BoxFit.cover,
                                        image:
                                            getImageProvider(File(stadiumImg)))
                                    : null,
                              ),
                            ),
                          ),
                        ),
                      ),
//Stadium image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 35.0),
                          child: Container(
                            width: 130.0,
                            height: 130.0,
                            decoration: new BoxDecoration(
                              color: AppColors.white,
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: stadiumImg != null
                                    ? getImageProvider(File(stadiumImg))
                                    : new AssetImage("images/tennis-court.png"),
                              ),
                              // border: Border.all(
                              //   color: Colors.black,
                              //   width: 3.0,
                              // ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
//name--------------------------------
                  new Container(
                    width: 300.0,
                    child: new Text(
                      'Name',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(stdName),
                    ),
                  ),

//Address-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Address',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(stadiumAddress),
                    ),
                  ),

// contact no-------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Contact NO',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(stadiumConNo),
                    ),
                  ),

//No of Courts-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'No of Courts',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(stadiumNoCouet),
                    ),
                  ),

//Reservation URL---------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Reservation URL',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(stadiumResURL),
                    ),
                  ),

//GPS Location---------------------------
                  _mapDisplay
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new Container(
                            width: 300.0,
                            child: new Text(
                              'Map',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  _mapDisplay
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: 300,
                            height: 300,
                            child: GoogleMap(
                              onMapCreated: _onMapCreated,
                              initialCameraPosition: CameraPosition(
                                target: _mapLocation,
                                zoom: 14,
                              ),
                              markers: Set<Marker>.of(markers.values),
                              zoomGesturesEnabled: false,
                              myLocationEnabled: false,
                              tiltGesturesEnabled: false,
                              rotateGesturesEnabled: false,
                              scrollGesturesEnabled: false,
                              compassEnabled: false,
                              myLocationButtonEnabled: false,
                            ),
                          ),
                        )
                      : Container(),
//OK button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: AppColors.ternary_color,
                          onPressed: () {
                            widget.onSelected(RoutingData.Stadium, true, false);
                          },
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "OK",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  // if file not found set default
  ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(f)
        : const AssetImage("images/tennis-court.png");
  }

  //check available for set background image
  bool checkImageAvailability(File f) {
    return f.existsSync() ? true : false;
  }

  @override
  resultFunction(func, subFunc, response) {}
}
