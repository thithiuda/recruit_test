import 'package:tenizo/custom/common_popup_onebutton.dart';
import 'package:tenizo/layout/MainPage.dart';
import 'package:tenizo/layout/user_registration.dart';
import 'package:tenizo/model/color_loader.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tenizo/validation/textBoxValidation.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with BaseControllerListner {
  BaseController controller;

  final List<Widget> _painters = <Widget>[];
  final List<Widget> _facebook = <Widget>[];
  final List<Widget> _google = <Widget>[];
  final List<Widget> _instagram = <Widget>[];

  TextEditingController _usernameFieldController = TextEditingController();
  TextEditingController _passwordFieldController = TextEditingController();

  bool _loading = true;

  //validation variables

  bool _validateName = true;
  bool _validatePassword = true;

  static String messageName = "Error in text";
  static String messagePassword = "Error in text";

  @override
  void initState() {
    super.initState();
    checkEnabled();

    controller = new BaseController(this);

    _loadData();

    _painters.add(
      SvgPicture.asset('images/lock.svg'),
    );
    _facebook.add(
      SvgPicture.asset('images/facebook.svg'),
    );
    _google.add(
      SvgPicture.asset('images/google.svg'),
    );
    _instagram.add(
      SvgPicture.asset('images/instagram.svg'),
    );
  }

  _loadData() {
    var param = [
      "SELECT * FROM user where is_login = ? ",
      [1],
      {"calledMethod": 'userData'}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param);
  }

  _loginCheck() {
    var userName = _usernameFieldController.text;

    var param1 = [
      "SELECT password FROM user  where user_name = ?",
      [userName],
      {"calledMethod": 'userPassword'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

  checkPassword(data) {
    if (data.length == 0) {
      showDialog(
          context: context,
          builder: (context) => CommonPopupOneButton(
              context, "error", "There is no such User Name.", [], _clearText));
    } else {
      var passowrData = data[0]['password'];
      var currentPassword = _passwordFieldController.text;
      var userName = _usernameFieldController.text;

      if (currentPassword == passowrData) {
        var param1 = [
          "UPDATE user SET is_login = ? where user_name = ?",
          [1, userName],
          {"calledMethod": 'changeLogin'}
        ];

        controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
        UserDetails.username = _usernameFieldController.text.toString();
        Navigator.pushReplacementNamed(context, "/MainPage");
        
        
      } else {
        showDialog(
            context: context,
            builder: (context) => CommonPopupOneButton(
                context, "error", "Password is Incorrect.", [], _clearText));
      }
    }
  }

  _clearText(List<String> paramList) {
    _usernameFieldController.clear();
    _passwordFieldController.clear();
  }

  //Check the validation when click the button ------------
  checkValidation() {
//User Name --------------
    var errorStatusName =
        TextBoxValidation.isEmpty(_usernameFieldController.text);
    setState(() {
      _validateName = errorStatusName['state'];
      messageName = errorStatusName['errorMessage'];
    });

//Password----------
    var errorStatusPassword =
        TextBoxValidation.isEmpty(_passwordFieldController.text);

    if (errorStatusPassword['state'] == false) {
      setState(() {
        _validatePassword = errorStatusPassword['state'];
        messagePassword = errorStatusPassword['errorMessage'];
      });
    }
  }

  // DISABLED BTN--------------------------
  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

  void checkEnabled() {
    if ((_usernameFieldController.text.toString() != '') &&
        (_passwordFieldController.text.toString() != '')) {
      setState(() {
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var buttonMargin = EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0);
  var imgWidth = 140.0;
  var imgHeight = 140.0;

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      buttonMargin = EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0);
    } else {}

    if (deviceHeight <= 500) {
      imgWidth = 80.0;
      imgHeight = 80.0;
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
          child: _loading
          ? Container(color: Colors.white, child: Center(child: ColorLoader()))
          : Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                title: MediaQuery(
                  data: MediaQueryData(),
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                centerTitle: true,
                backgroundColor: AppStyle.color_Head,
              ),
              backgroundColor: Colors.white,
              body: MediaQuery(
                data: MediaQueryData(),
                child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 30.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
// logo------------------
                        Container(
                          child: Hero(
                            tag: 'hero',
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 70.0, right: 70),
                              child: new Container(
                                width: imgWidth,
                                height: imgHeight,
                                decoration: BoxDecoration(),
                                child: GridView.extent(
                                  shrinkWrap: true,
                                  maxCrossAxisExtent: 210,
                                  children: _painters.toList(),
                                ),
                              ),
                            ),
                          ),
                        ),

// email----------------------

                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: TextField(
                            controller: _usernameFieldController,
                            onChanged: (value) {
                              var errorStatus = TextBoxValidation.isEmpty(
                                  _usernameFieldController.text);

                              setState(() {
                                _validateName = errorStatus['state'];
                                messageName = errorStatus['errorMessage'];
                              });

                              checkEnabled();
                            },
                            cursorColor: AppStyle.color_TextGreen,
                            decoration: new InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStyle.color_Head, width: 2.0),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStyle.color_Head, width: 2.0),
                              ),
                              errorText: _validateName ? null : messageName,
                              errorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                      width: 1.2, color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                      width: 1.2, color: Colors.red)),
                              labelText: 'User Name : ',
                              labelStyle: new TextStyle(
                                color: AppStyle.color_TextGreen,
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                        ),

// password----------------------------------

                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: TextField(
                            controller: _passwordFieldController,
                            onChanged: (value) {
                              var errorStatus = TextBoxValidation.isEmpty(
                                  _passwordFieldController.text);

                              setState(() {
                                _validatePassword = errorStatus['state'];
                                messagePassword = errorStatus['errorMessage'];
                              });

                              checkEnabled();
                            },
                            cursorColor: AppStyle.color_TextGreen,
                            obscureText: true,
                            decoration: new InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStyle.color_Head, width: 2.0),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppStyle.color_Head, width: 2.0),
                              ),
                              errorText:
                                  _validatePassword ? null : messagePassword,
                              errorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                      width: 1.2, color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                      width: 1.2, color: Colors.red)),
                              labelText: 'Password : ',
                              labelStyle: new TextStyle(
                                color: AppStyle.color_TextGreen,
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                        ),

                        SizedBox(height: 40.0),

// loginButton-----------------------

                        Padding(
                          padding: const EdgeInsets.only(top: 0.0),
                          child: new Container(
                            width: 200,
                            height: 40,
                            margin: buttonMargin,
                            child: new RaisedButton(
                                padding: EdgeInsets.only(
                                    top: 3.0, bottom: 3.0, left: 3.0),
                                color: disabledBtn,
                                onPressed:
                                    (_usernameFieldController.text != "" &&
                                            _passwordFieldController.text != "")
                                        ? () => _loginCheck()
                                        : () => checkValidation(),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide(color: Colors.black)),
                                child: new Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Container(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        child: new Text(
                                          "Login",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        )),
                                  ],
                                )),
                          ),
                        ),

// facebookButton------------------------------

                        // Padding(
                        //   padding: const EdgeInsets.only(top: 10.0),
                        //   child: SizedBox(
                        //       width: 200,
                        //       height: 40,
                        //       child: RaisedButton(
                        //           color: Colors.white,
                        //           onPressed: () {
                        //             showDialog(
                        //                 context: context,
                        //                 builder: (context) =>
                        //                     CommonPopupOneButton(
                        //                         context,
                        //                         "notification",
                        //                         "This is the Notification",
                        //                         [],
                        //                         _clearText));
                        //           },
                        //           shape: new RoundedRectangleBorder(
                        //               borderRadius:
                        //                   new BorderRadius.circular(30.0),
                        //               side: BorderSide(color: Colors.black)),
                        //           child: new Row(
                        //             mainAxisAlignment: MainAxisAlignment.start,
                        //             children: <Widget>[
                        //               SizedBox(
                        //                 child: Container(
                        //                   decoration: BoxDecoration(
                        //                     border: Border(
                        //                       right: BorderSide(
                        //                         color: Colors.black,
                        //                         width: 1.0,
                        //                       ),
                        //                     ),
                        //                   ),
                        //                   child: Padding(
                        //                     padding: const EdgeInsets.only(
                        //                         left: 0.0, right: 8, top: 7),
                        //                     child: new Container(
                        //                       width: 25,
                        //                       height: 40,
                        //                       decoration: BoxDecoration(),
                        //                       child: GridView.extent(
                        //                         maxCrossAxisExtent: 200,
                        //                         children: _facebook.toList(),
                        //                       ),
                        //                     ),
                        //                   ),
                        //                 ),
                        //               ),
                        //               Padding(
                        //                 padding:
                        //                     const EdgeInsets.only(left: 12),
                        //                 child: Text(
                        //                   "Facebook Login.",
                        //                   textAlign: TextAlign.center,
                        //                   style: TextStyle(
                        //                       color: Colors.black,
                        //                       fontWeight: FontWeight.w600,
                        //                       fontSize: 16),
                        //                 ),
                        //               ),
                        //             ],
                        //           ))),
                        // ),
// googleButton----------------------

                        // Padding(
                        //   padding: const EdgeInsets.only(top: 10.0),
                        //   child: SizedBox(
                        //       width: 200,
                        //       height: 40,
                        //       child: RaisedButton(
                        //           color: Colors.white,
                        //           onPressed: () {
                        //             showDialog(
                        //                 context: context,
                        //                 builder: (context) =>
                        //                     CommonPopupOneButton(
                        //                         context,
                        //                         "error",
                        //                         "This is the Error",
                        //                         [],
                        //                         _clearText));
                        //           },
                        //           shape: new RoundedRectangleBorder(
                        //               borderRadius:
                        //                   new BorderRadius.circular(30.0),
                        //               side: BorderSide(color: Colors.black)),
                        //           child: new Row(
                        //             mainAxisAlignment: MainAxisAlignment.start,
                        //             children: <Widget>[
                        //               SizedBox(
                        //                 child: Container(
                        //                   decoration: BoxDecoration(
                        //                     border: Border(
                        //                       right: BorderSide(
                        //                         color: Colors.black,
                        //                         width: 1.0,
                        //                       ),
                        //                     ),
                        //                   ),
                        //                   child: Padding(
                        //                     padding: const EdgeInsets.only(
                        //                         left: 0.0, right: 8, top: 7),
                        //                     child: new Container(
                        //                       width: 25,
                        //                       height: 40,
                        //                       decoration: BoxDecoration(),
                        //                       child: GridView.extent(
                        //                         maxCrossAxisExtent: 200,
                        //                         children: _google.toList(),
                        //                       ),
                        //                     ),
                        //                   ),
                        //                 ),
                        //               ),
                        //               Padding(
                        //                 padding:
                        //                     const EdgeInsets.only(left: 12),
                        //                 child: Text(
                        //                   "Google Login",
                        //                   style: TextStyle(
                        //                       color: Colors.black,
                        //                       fontWeight: FontWeight.w600,
                        //                       fontSize: 16),
                        //                 ),
                        //               ),
                        //             ],
                        //           ))),
                        // ),

//instergram button --------------------------

                        // Padding(
                        //   padding: const EdgeInsets.only(top: 10.0, bottom: 10),
                        //   child: SizedBox(
                        //       width: 200,
                        //       height: 40,
                        //       child: RaisedButton(
                        //           color: Colors.white,
                        //           onPressed: () {},
                        //           shape: new RoundedRectangleBorder(
                        //               borderRadius:
                        //                   new BorderRadius.circular(30.0),
                        //               side: BorderSide(color: Colors.black)),
                        //           child: new Row(
                        //             mainAxisAlignment: MainAxisAlignment.start,
                        //             children: <Widget>[
                        //               SizedBox(
                        //                 child: Container(
                        //                   decoration: BoxDecoration(
                        //                     border: Border(
                        //                       right: BorderSide(
                        //                         color: Colors.black,
                        //                         width: 1.0,
                        //                       ),
                        //                     ),
                        //                   ),
                        //                   child: Padding(
                        //                     padding: const EdgeInsets.only(
                        //                         left: 0.0, right: 8, top: 7),
                        //                     child: new Container(
                        //                       width: 25,
                        //                       height: 40,
                        //                       decoration: BoxDecoration(),
                        //                       child: GridView.extent(
                        //                         maxCrossAxisExtent: 200,
                        //                         children: _instagram.toList(),
                        //                       ),
                        //                     ),
                        //                   ),
                        //                 ),
                        //               ),
                        //               Padding(
                        //                 padding:
                        //                     const EdgeInsets.only(left: 12),
                        //                 child: Text(
                        //                   "Instagram Login",
                        //                   style: TextStyle(
                        //                       color: Colors.black,
                        //                       fontWeight: FontWeight.w600,
                        //                       fontSize: 16),
                        //                 ),
                        //               ),
                        //             ],
                        //           ))),
                        // ),

                        SizedBox(
                          height: 70,
                        ),

// forgotLabel---------------------

                        Column(
                          children: <Widget>[
                            FlatButton(
                              child: RichText(
                                text: new TextSpan(
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14),
                                  children: <TextSpan>[
                                    new TextSpan(
                                        text: 'Don\'t you have account?'),
                                    new TextSpan(
                                        text: ' Sign Up Here',
                                        style: new TextStyle(
                                            color: AppStyle.color_TextGreen,
                                            fontFamily: 'Rajdhani',
                                            fontWeight: FontWeight.w500)),
                                  ],
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UserRegistration()),
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (subFunc == ControllerSubFunc.db_select) {
            if (response["calledMethod"] == "userPassword") {
              checkPassword(response['response_data']);
            } else if (response["calledMethod"] == "userData") {
              if (response['response_data'].length != 0) {
                UserDetails.username = response['response_data'][0]['user_name'];
                Navigator.pushReplacementNamed(context,"/MainPage",);
                
                
              } else {
                setState(() {
                  _loading = false;
                });
              }
            } else {}
          }

          break;
        }
      default:
    }
  }
}
