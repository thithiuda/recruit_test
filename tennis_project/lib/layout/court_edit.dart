import 'dart:io';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tenizo/custom/commom_popup.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:camera/camera.dart';

class CourtEdit extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _CourtEditState createState() => _CourtEditState();
  CourtEdit({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _CourtEditState extends State<CourtEdit> with BaseControllerListner {
  BaseController controller;

  TextEditingController _nameFieldController = TextEditingController();

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  List<DropdownMenuItem<String>> _indoorDropDownMenuItems;

  List _surface = [
    CommonValues.notSet,
    CommonValues.surfaceClay,
    CommonValues.surfaceHard,
    CommonValues.surfaceGrass,
    CommonValues.surfaceCarpet
  ];
  List _indoor = [
    CommonValues.notSet,
    CommonValues.indoor,
    CommonValues.outdoor
  ];

  Future<File> imageFile;
  File pickedImage;
  var deleteImage;

  String sqlData;
  String _currentSurface;
  String _currentIndoor;
  String name = "";
  String _imageFilePath;
  bool _imageAvailability;

  var param = [
    "UPDATE court SET court_name = ?,serface = ?,indoor = ?,available_time = ?, court_image = ? where court_id = ?",
    newData
  ];

  static var newData = ['', '', '', '', '', ''];

  static var courtId = "";
  static var courtName = "";
  static var courtSurface = "";
  static var courtIndoor = "";
  static var courtTime = "";
  static var avilableTime = "";
  static var courtImg;

  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

  // INITIALIZING STATE VALUES --------------------------------------------
  static var timeObjectfrom = DateTime.now();
  static var timeObjectto = DateTime.now();

  String displayedTimeFrom = "00 : 00";
  String displayedTimeTo = "00 : 00";

  String dbTimeFrom = "00 : 00";
  String dbTimeTo = "00 : 00";

  String initTimeFrom = "00 : 00";
  String initTimeTo = "00 : 00";

  //Validation variables
  bool _validateName = true;
  static String messageName = "Error in text";

  // DISABLED BTN--------------------------
  void checkEnabled() {
    if ((_nameFieldController.text.toString() != '' && _validateName == true)) {
      setState(() {
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

  // Time FORMAT (eg: 00:00) --------------------------------------------

  static timeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr : $min';
  }

  static dbtimeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr:$min';
  }

//Save values to db
  saveValues(List<String> paramList) async {
    if (_nameFieldController.text != "" && _validateName == true) {
      if (pickedImage != null) {
        await saveImage(pickedImage); // for save image
      } else {
        if (_imageFilePath == null) {
          await _deleteOldImage(deleteImage);
        }
      }
      avilableTime = dbTimeFrom + " " + "-" + " " + dbTimeTo;

      setState(() => newData[0] = _nameFieldController.text);
      setState(() => newData[1] = _currentSurface);
      setState(() => newData[2] = _currentIndoor);
      setState(() => newData[3] = avilableTime);
      setState(() => newData[4] = _imageFilePath);
      setState(() => newData[5] = courtId);

      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param);
    } else {}
  }

  @override
  void initState() {
    super.initState();

    if (widget.arguments['courtId'].toString() != "") {
      setState(() {
        courtId = widget.arguments['courtId'].toString();
        courtName = widget.arguments['courtName'].toString();
        courtSurface = widget.arguments['courtSurface'].toString();
        courtIndoor = widget.arguments['courtIndoor'].toString();
        courtTime = widget.arguments['courtTime'].toString();
        courtImg = widget.arguments['courtImage'];
      });
    }

    //set data to the screen
    var availableTime = courtTime.split('-');

    _nameFieldController.text = courtName;
    displayedTimeFrom = availableTime[0];
    displayedTimeTo = availableTime[1];

    dbTimeFrom = availableTime[0];
    dbTimeTo = availableTime[1];

    initTimeFrom = availableTime[0];
    initTimeTo = availableTime[1];

    _dropDownMenuItems = getDropDownMenuItems();
    _indoorDropDownMenuItems = getDropDownIndoorItems();

    _currentSurface = courtSurface;
    _currentIndoor = courtIndoor;
    _imageFilePath = courtImg;
    deleteImage = courtImg;

    checkEnabled();
    controller = new BaseController(this);

    //Check image availaibilty
    if (courtImg != null) {
      setState(() {
        _imageAvailability = checkImageAvailability(File(courtImg));
      });
    } else {
      setState(() {
        _imageAvailability = false;
      });
    }
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String surface in _surface) {
      items.add(
        new DropdownMenuItem(
          value: surface,
          child: new Text(surface),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownIndoorItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String indoor in _indoor) {
      items.add(
        new DropdownMenuItem(
          value: indoor,
          child: new Text(indoor),
        ),
      );
    }
    return items;
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

//image remove/edit popup functions
  void choiceAction(choice) async {
    if (choice == "Take") {
      var img = await pickImageFromCamGallery(ImageSource.camera);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Choose") {
      var img = await pickImageFromCamGallery(ImageSource.gallery);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Remove") {
      removeImage();
    }
  }

  DateTime timeTo;
  DateTime timeFrom;

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    if (displayedTimeTo == initTimeTo) {
      timeTo = DateTime.now();
    } else {
      timeTo = DateTime.parse(timeObjectto.toString());
    }

    if (displayedTimeFrom == initTimeFrom) {
      timeFrom = DateTime.now();
    } else {
      timeFrom = DateTime.parse(timeObjectfrom.toString());
    }

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Container(
                                    child: Image.file(
                                      snapshot.data,
                                      fit: BoxFit.cover,
                                      width: double.infinity,
                                      height: 200.0,
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Container(
                                    height: 200.0,
                                    child: Container(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 0.0),
                                        child: Container(
                                          width: double.infinity,
                                          height: 200,
                                          decoration: BoxDecoration(
                                            color: AppColors.gray,
                                            image: _imageAvailability
                                                ? DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: getImageProvider(
                                                        File(courtImg)))
                                                : null,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),
//cover of the image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 30.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: new FileImage(snapshot.data),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'Choose',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          8.0),
                                                              child: Icon(
                                                                Icons.camera,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text(
                                                                'Choose Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      pickedImage != null
                                                          ? PopupMenuItem<
                                                                  String>(
                                                              value: 'Remove',
                                                              child: SizedBox
                                                                  .expand(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              10.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .remove_circle,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        'Remove Image'),
                                                                  ],
                                                                ),
                                                              ))
                                                          : null,
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 35.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        color: AppColors.white,
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: courtImg != null
                                              ? getImageProvider(File(courtImg))
                                              : new AssetImage(
                                                  "images/tennis-court.png"),
                                        ),
                                        // border: Border.all(
                                        //   color: Colors.black,
                                        //   width: 3.0,
                                        // ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'Choose',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons.camera,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text(
                                                                'Choose Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      _imageFilePath != null
                                                          ? PopupMenuItem<
                                                                  String>(
                                                              value: 'Remove',
                                                              child: SizedBox
                                                                  .expand(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              10.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .remove_circle,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        'Remove Image'),
                                                                  ],
                                                                ),
                                                              ))
                                                          : null,
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),

//profile name----------------------------
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 165.0 ,left: 20,right: 20),
                          child: Container(
                            height: 30.0,
                            child: Text(
                              courtName,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
//name--------------------------------
                  new Container(
                    width: 300.0,
                    child: new Text(
                      'Name / NO',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 25,
                        cursorColor: Colors.black,
                        controller: _nameFieldController,
                        onChanged: (value) {
                          setState(() {
                            courtName = value;
                          });

                          var errorStatus = TextBoxValidation.isEmpty(
                              _nameFieldController.text);

                          setState(() {
                            _validateName = errorStatus['state'];
                            messageName = errorStatus['errorMessage'];
                          });

                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          counterText: "",
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _nameFieldController.clear();
                              courtName = "";

                              var errorStatus = TextBoxValidation.isEmpty(
                                  _nameFieldController.text);

                              setState(() {
                                _validateName = errorStatus['state'];
                                messageName = errorStatus['errorMessage'];
                              });

                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateName ? null : messageName,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

// Court Surface:-------------------------

                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 0.0, bottom: 8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 10.0, right: 0, left: 0.0),
                                  child: new Container(
                                    width: 300,
                                    child: new Text(
                                      'Court Surface',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  height: 48,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: AppColors.form_border,
                                      style: BorderStyle.solid,
                                      width: 1.2,
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Container(
                                          width: 50,
                                          height: 50,
                                          child: Center(
                                            child: Icon(Icons.expand_more,
                                                size: 24, color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: new DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              value: _currentSurface,
                                              items: _dropDownMenuItems,
                                              onChanged: surfaceDropDownItem,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Rajdhani'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

//  Indoor Outdoor:-------------------------

                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 0.0, bottom: 8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 10.0, right: 0, left: 0.0),
                                  child: new Container(
                                    width: 300,
                                    child: new Text(
                                      'Indoor / Outdoor',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  height: 48,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: AppColors.form_border,
                                      style: BorderStyle.solid,
                                      width: 1.2,
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Container(
                                          width: 50,
                                          height: 50,
                                          child: Center(
                                            child: Icon(Icons.expand_more,
                                                size: 24, color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: new DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              value: _currentIndoor,
                                              items: _indoorDropDownMenuItems,
                                              onChanged: indoorDropDownItem,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Rajdhani'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

//Available Time-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Available Time',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 0),
                    child: Container(
                      width: 300,
                      child: Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              new Align(
                                child: Container(
                                  width: 132,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                          width: double.infinity,
                                          height: 48,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4.0),
                                            border: Border.all(
                                              color: AppColors.form_border,
                                              width: 1.2,
                                              style: BorderStyle.solid,
                                            ),
                                          ),
                                          child: FlatButton(
                                            onPressed: () {
                                              showCupertinoModalPopup<void>(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return _buildBottomPicker(
                                                    CupertinoDatePicker(
                                                      use24hFormat: true,
                                                      mode:
                                                          CupertinoDatePickerMode
                                                              .time,
                                                      initialDateTime: timeFrom,
                                                      onDateTimeChanged:
                                                          (date) {
                                                        setState(() {
                                                          timeObjectfrom = date;
                                                          displayedTimeFrom =
                                                              timeFormatter(
                                                                  date);
                                                          dbTimeFrom =
                                                              dbtimeFormatter(
                                                                  date);
                                                        });
                                                      },
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Center(
                                              child: Text(
                                                displayedTimeFrom,
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: AppColors.black,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, left: 15.0),
                            child: new Container(
                              width: 20.0,
                              child: new Text(
                                '-',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              new Align(
                                child: Container(
                                  width: 132,
                                  // height: row_height,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                          width: double.infinity,
                                          height: 48,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4.0),
                                            border: Border.all(
                                              color: AppColors.form_border,
                                              width: 1.2,
                                              style: BorderStyle.solid,
                                            ),
                                          ),
                                          child: FlatButton(
                                            onPressed: () {
                                              showCupertinoModalPopup<void>(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return _buildBottomPicker(
                                                    CupertinoDatePicker(
                                                      use24hFormat: true,
                                                      mode:
                                                          CupertinoDatePickerMode
                                                              .time,
                                                      initialDateTime: timeTo,
                                                      onDateTimeChanged:
                                                          (date) {
                                                        setState(() {
                                                          timeObjectto = date;
                                                          displayedTimeTo =
                                                              timeFormatter(
                                                                  date);
                                                          dbTimeTo =
                                                              dbtimeFormatter(
                                                                  date);
                                                        });
                                                      },
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Center(
                                              child: Text(
                                                displayedTimeTo,
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: AppColors.black,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),

//Save button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: disabledBtn,
                          onPressed: (_nameFieldController.text != "")
                              ? () => showDialog(
                                  context: context,
                                  builder: (context) => CommonPopup(
                                      context, "edit", [], saveValues))
                              : () {},
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: disabledBtnFont,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "Save",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

//surface selection
  void surfaceDropDownItem(String selectedSurface) {
    setState(() {
      _currentSurface = selectedSurface;
    });
  }

  //country code selection
  void indoorDropDownItem(String selectedIndoor) {
    setState(() {
      _currentIndoor = selectedIndoor;
    });
  }

  //Open gallery
  pickImageFromCamGallery(ImageSource source) async {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
    return imageFile;
  }

  //Save image to gallery
  saveImage(img) async {
    final directory = await getApplicationDocumentsDirectory();
    // final String pathx = directory.path;
    final String pathx = '${directory.path}/tennis/pictures'; //new directory
    await Directory(pathx).create(recursive: true);

    String path = img.path;
    if (path != "" && path != null) {
      var savedFile = File.fromUri(Uri.file(path));

      _deleteOldImage(deleteImage); //delete old image
      var randomNumber = Uuid().v1();
      var iName = randomNumber.toString() + ".png"; //for image

      final imagePath = '$pathx/$iName';
      File newImageFile = File(imagePath);
      var finalSavedFile =
          await newImageFile.writeAsBytes(await savedFile.readAsBytes());

      setState(() {
        _imageFilePath = finalSavedFile.path;
      });
    }
  }

  //Remove image
  removeImage() {
    setState(() {
      imageFile = null; //remove both images, When image recently selected,
      courtImg = null; // for player imge
      _imageAvailability = false; //for bg image
      _imageFilePath = null; //DB
      pickedImage = null; //for delte
    });
  }

  //delete image from directory
  _deleteOldImage(courtImage) async {
    if (courtImage != null) {
      final delFir = File(courtImage);
      if (delFir.existsSync()) {
        delFir.deleteSync(recursive: true); //for delete
      }
    }
  }

  // if file not found set default
  ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(f)
        : const AssetImage("images/tennis-court.png");
  }

  //check available for set background image
  bool checkImageAvailability(File f) {
    return f.existsSync() ? true : false;
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            if (response['response_data'].toString() != "null") {
              setState(() {
                if (response['response_data'].toString() != "[]") {
                  sqlData = response['response_data'].toString();
                } else {
                  sqlData = "null";
                }
              });
            }

            widget.onSelected(RoutingData.Stadium, true, false);
          }
          break;
        }
      default:
        {
          //Do nothing
        }
    }
  }

  Widget _buildBottomPicker(Widget picker) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 44,
                child: CupertinoButton(
                  pressedOpacity: 0.3,
                  padding: EdgeInsets.only(right: 16, top: 0),
                  child: Text(
                    'Done',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 200,
          padding: const EdgeInsets.only(top: 0.0),
          color: CupertinoColors.white,
          child: DefaultTextStyle(
            style: const TextStyle(
              color: CupertinoColors.black,
              fontSize: 22.0,
            ),
            child: SafeArea(
              top: true,
              child: picker,
            ),
          ),
        ),
      ],
    );
  }
}
