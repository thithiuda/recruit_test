import 'dart:io';
import 'dart:ui';
import 'package:tenizo/custom/common_popup_onebutton.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:tenizo/util/user_registration_util.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:camera/camera.dart';

import 'MainPage.dart';

class UserRegistration extends StatefulWidget {
  @override
  _UserRegistrationState createState() => _UserRegistrationState();
}

class _UserRegistrationState extends State<UserRegistration>
    with BaseControllerListner {
  BaseController controller;

  TextEditingController _nameFieldController = TextEditingController();
  TextEditingController _usernameFieldController = TextEditingController();
  TextEditingController _passwordFieldController = TextEditingController();
  TextEditingController _confirmPasswordFieldController =
      TextEditingController();

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  List<DropdownMenuItem<String>> _userTypedropDownMenuItems;

  List _gender = ["Male", "Female"];
  List _userType = ["Parent", "Coach", "Other"];

  Future<File> imageFile;
  File pickedImage;

  String sqlData;
  String _currentGender;
  String _currentUsertype;
  String displayedDate = UserRegistrationPageUtils.dateFormatter(dateObject);
  String name = "";
  String _imageFilePath;

  var param = [
    "INSERT INTO user (name,user_name,password,gender,date_of_birth,user_type,user_image,is_login) VALUES (? , ? , ?, ? , ? , ?, ?, ?)",
    newData,
    {"calledMethod": 'userDataInsert'}
  ];

  var param2 = ["SELECT * FROM user", []];

  bool pressAttention = false;

  static var newData = ['', '', '', '', '', '', '', 1];
  static var dateObject = DateTime.now();
  static var today = DateTime.now();

  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

  //Validation variables
  bool _validatePassword = true;
  bool _validateConfirmPassword = true;
  bool _validateName = true;
  bool _validateUserNme = true;

  static String messagePassword = "Error in text";
  static String messageConfirmPassword = "Error in text";
  static String messageName = "Error in text";
  static String messageUserName = "Error in text";

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  // DISABLED BTN--------------------------
  void checkEnabled() {
    if ((_nameFieldController.text.toString() != '') &&
        (_usernameFieldController.text.toString() != '') &&
        (_passwordFieldController.text.toString() != '') &&
        (_confirmPasswordFieldController.text.toString() != '') &&
        _validatePassword == true &&
        _validateConfirmPassword == true &&
        _validateName == true &&
        _validateUserNme == true) {
      setState(() {
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

//Check user name is already used
  checkUserNmae(data) {
    var param1 = [
      "SELECT * FROM user where user_name = ?",
      [data],
      {"calledMethod": 'userData'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

//Save values to db
  saveValues() async {
    if (_nameFieldController.text != "" &&
        _usernameFieldController.text != "" &&
        _passwordFieldController.text != "" &&
        _confirmPasswordFieldController.text != "" &&
        _validatePassword == true &&
        _validateConfirmPassword == true &&
        _validateName == true &&
        _validateUserNme == true) {
      if (pickedImage != null) {
        await saveImage(pickedImage); // for save image
      }
      setState(() => newData[0] = _nameFieldController.text);
      setState(() => newData[1] = _usernameFieldController.text);
      setState(() => newData[2] = _passwordFieldController.text);
      setState(() => newData[3] = _currentGender);

      setState(() => newData[4] = displayedDate);
      setState(() => newData[5] = _currentUsertype);
      setState(() => newData[6] = _imageFilePath);

      controller.execFunction(ControllerFunc.db_sqlite, ControllerSubFunc.db_insert, param);
      UserDetails.username = _usernameFieldController.text.toString();
      
      Navigator.pushReplacementNamed(context, "/MainPage");

      // Navigator.push(
      //             context,
      //             MaterialPageRoute(builder: (context) => MainPage(selectedPage: RoutingData.Home,userName:_usernameFieldController.text)),
      //           );
    } else {}
  }

  @override
  void initState() {
    super.initState();

    _dropDownMenuItems = getDropDownMenuItems();
    _userTypedropDownMenuItems = getUserTypeDropDownMenuItems();

    _currentGender = _dropDownMenuItems[0].value;
    _currentUsertype = _userTypedropDownMenuItems[0].value;

    controller = new BaseController(this);
    checkEnabled();
  }

//Dropdown data
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String gender in _gender) {
      items.add(
        new DropdownMenuItem(
          value: gender,
          child: new Text(gender),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<String>> getUserTypeDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String userType in _userType) {
      items.add(
          new DropdownMenuItem(value: userType, child: new Text(userType)));
    }
    return items;
  }

//Set data according to the curren devise
  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

  //image remove/edit popup functions
  void choiceAction(choice) async {
    if (choice == "Take") {
      var img = await pickImageFromCamGallery(ImageSource.camera);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Choose") {
      var img = await pickImageFromCamGallery(ImageSource.gallery);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Remove") {
      setState(() {
        imageFile = null;
        _imageFilePath = null;
        pickedImage = null;
      });
    }
  }

  //gendert selection
  void genderDropDownItem(String selectedGender) {
    setState(() {
      _currentGender = selectedGender;
    });
  }

//user type selection
  void usertypeDropDownItem(String selectedUsertype) {
    setState(() {
      _currentUsertype = selectedUsertype;
    });
  }

  //Open gallery
  pickImageFromCamGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
    return imageFile;
  }

  saveImage(File img) async {
    final directory = await getApplicationDocumentsDirectory();
    // final String pathx = directory.path;
    final String pathx = '${directory.path}/tennis/pictures'; //new directory
    await Directory(pathx).create(recursive: true);

    String path = img.path;
    if (path != "" && path != null) {
      var savedFile = File.fromUri(Uri.file(path));

      var randomNumber = Uuid().v1();
      var iName = randomNumber.toString() + ".png"; //for image

      final imagePath = '$pathx/$iName';
      File newImageFile = File(imagePath);
      var finalSavedFile =
          await newImageFile.writeAsBytes(await savedFile.readAsBytes());

      setState(() {
        _imageFilePath = finalSavedFile.path;
      });
    }
  }

//Clear all text in once
  _clearText(List<String> paramList) {
    _usernameFieldController.clear();
    _passwordFieldController.clear();
    _confirmPasswordFieldController.clear();
  }

//Check the validation when click the button ------------
  checkValidation() {
//Name --------------
    var errorStatusName = TextBoxValidation.isEmpty(_nameFieldController.text);
    setState(() {
      _validateName = errorStatusName['state'];
      messageName = errorStatusName['errorMessage'];
    });

//User Name --------------
    var errorStatusUserName =
        TextBoxValidation.isEmpty(_usernameFieldController.text);

    setState(() {
      _validateUserNme = errorStatusUserName['state'];
      messageUserName = errorStatusUserName['errorMessage'];
    });

//Password----------
    var errorStatusPassword =
        TextBoxValidation.isEmpty(_passwordFieldController.text);

    if (errorStatusPassword['state'] == false) {
      setState(() {
        _validatePassword = errorStatusPassword['state'];
        messagePassword = errorStatusPassword['errorMessage'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return Scaffold(
      appBar: AppBar(
        title: MediaQuery(
            data: MediaQueryData(),
            child: Text('Register User',
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold))),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
      ),
      body: WillPopScope(
        child: MediaQuery(
          data: MediaQueryData(),
          child: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
//User backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Container(
                                    child: Image.file(
                                      snapshot.data,
                                      fit: BoxFit.cover,
                                      width: double.infinity,
                                      height: 200.0,
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Container(
                                    height: 200.0,
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),
//User image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 30.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: new FileImage(snapshot.data),
                                        ),
                                        border: Border.all(
                                          color: Colors.black,
                                          width: 3.0,
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                          value: 'Choose',
                                                          child:
                                                              SizedBox.expand(
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                                  child: Icon(
                                                                    Icons
                                                                        .camera,
                                                                    size: 20,
                                                                  ),
                                                                ),
                                                                Text(
                                                                    'Choose Image'),
                                                              ],
                                                            ),
                                                          )),
                                                      pickedImage != null
                                                          ? PopupMenuItem<
                                                                  String>(
                                                              value: 'Remove',
                                                              child: SizedBox
                                                                  .expand(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              10.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .remove_circle,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        'Remove Image'),
                                                                  ],
                                                                ),
                                                              ))
                                                          : null,
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 30.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image:
                                              new AssetImage("images/user.png"),
                                        ),
                                        border: Border.all(
                                          color: Colors.black,
                                          width: 3.0,
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                          value: 'Choose',
                                                          child:
                                                              SizedBox.expand(
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                                  child: Icon(
                                                                    Icons
                                                                        .camera,
                                                                    size: 20,
                                                                  ),
                                                                ),
                                                                Text(
                                                                    'Choose Image'),
                                                              ],
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),

//profile name----------------------------
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 175.0),
                          child: Container(
                            height: 30.0,
                            child: Text(
                              name,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),

//name--------------------------------
                  new Container(
                    width: 300.0,
                    child: new Text(
                      'Name',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 25,
                        cursorColor: Colors.black,
                        controller: _nameFieldController,
                        onChanged: (value) {
                          setState(() {
                            name = value;
                          });

                          var errorStatus = TextBoxValidation.isEmpty(
                              _nameFieldController.text);

                          setState(() {
                            _validateName = errorStatus['state'];
                            messageName = errorStatus['errorMessage'];
                          });

                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          counterText: "",
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _nameFieldController.clear();

                              var errorStatus = TextBoxValidation.isEmpty(
                                  _nameFieldController.text);

                              setState(() {
                                _validateName = errorStatus['state'];
                                messageName = errorStatus['errorMessage'];
                              });

                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateName ? null : messageName,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

//user name-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'User Name',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 15,
                        cursorColor: Colors.black,
                        controller: _usernameFieldController,
                        onChanged: (String newValue) {
                          var errorStatus = TextBoxValidation.isEmpty(
                              _usernameFieldController.text);
                          setState(() {
                            _validateUserNme = errorStatus['state'];
                            messageUserName = errorStatus['errorMessage'];
                          });

                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          counterText: "",
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _usernameFieldController.clear();
                              var errorStatus = TextBoxValidation.isEmpty(
                                  _usernameFieldController.text);

                              if (errorStatus['state'] == false) {
                                setState(() {
                                  _validateUserNme = errorStatus['state'];
                                  messageUserName = errorStatus['errorMessage'];
                                });
                              }
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateUserNme ? null : messageUserName,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

//password---------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Password',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 15,
                        decoration: new InputDecoration(
                          counterText: "",
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _passwordFieldController.clear();

                              var errorStatus = TextBoxValidation.isEmpty(
                                  _passwordFieldController.text);

                              if (errorStatus['state'] == false) {
                                setState(() {
                                  _validatePassword = errorStatus['state'];
                                  messagePassword = errorStatus['errorMessage'];
                                });
                              }
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validatePassword ? null : messagePassword,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        cursorColor: Colors.black,
                        obscureText: true,
                        controller: _passwordFieldController,
                        onChanged: (String newValue) {
                          var errorStatus = [
                            TextBoxValidation.isEmpty(
                                _passwordFieldController.text),
                            TextBoxValidation.isPasswordValidateNew(
                                _passwordFieldController.text),
                            TextBoxValidation.isLengthMatch(
                                _passwordFieldController.text, 6)
                          ];
                          if (errorStatus[0]['state'] == false) {
                            setState(() {
                              _validatePassword = errorStatus[0]['state'];
                              messagePassword = errorStatus[0]['errorMessage'];
                            });
                          }

                          if (errorStatus[0]['state'] == true) {
                            setState(() {
                              _validatePassword = errorStatus[1]['state'];
                              messagePassword = errorStatus[1]['errorMessage'];
                            });
                          }
                          if (errorStatus[1]['state'] == true) {
                            _validatePassword = errorStatus[2]['state'];
                            messagePassword = errorStatus[2]['errorMessage'];
                          }
                          checkEnabled();
                        },
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

// confirm password---------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Confirm Password',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 15,
                        decoration: new InputDecoration(
                          counterText: "",
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _confirmPasswordFieldController.clear();

                              var errorStatus = TextBoxValidation.isEmpty(
                                  _confirmPasswordFieldController.text);

                              if (errorStatus['state'] == false) {
                                setState(() {
                                  _validateConfirmPassword =
                                      errorStatus['state'];
                                  messageConfirmPassword =
                                      errorStatus['errorMessage'];
                                });
                              }
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateConfirmPassword
                              ? null
                              : messageConfirmPassword,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        cursorColor: Colors.black,
                        obscureText: true,
                        controller: _confirmPasswordFieldController,
                        onChanged: (String newValue) {
                          var errorStatus = [
                            TextBoxValidation.isEmpty(
                                _confirmPasswordFieldController.text),
                            TextBoxValidation.isPasswordMatch(
                                _passwordFieldController.text,
                                _confirmPasswordFieldController.text)
                          ];
                          if (errorStatus[0]['state'] == false) {
                            setState(() {
                              _validateConfirmPassword =
                                  errorStatus[0]['state'];
                              messageConfirmPassword =
                                  errorStatus[0]['errorMessage'];
                            });
                          }

                          if (errorStatus[0]['state'] == true) {
                            setState(() {
                              _validateConfirmPassword =
                                  errorStatus[1]['state'];
                              messageConfirmPassword =
                                  errorStatus[1]['errorMessage'];
                            });
                          }
                          checkEnabled();
                        },
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

// Gender-------------------------

                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 0.0, bottom: 8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 10.0, right: 0, left: 0.0),
                                  child: new Container(
                                    width: 100,
                                    child: new Text(
                                      'Gender',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 100,
                                  height: 48,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: AppColors.form_border,
                                      style: BorderStyle.solid,
                                      width: 1.2,
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Container(
                                          width: 50,
                                          height: 48,
                                          child: Center(
                                            child: Icon(Icons.expand_more,
                                                size: 24, color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: new DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              value: _currentGender,
                                              items: _dropDownMenuItems,
                                              onChanged: genderDropDownItem,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Rajdhani'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),

// Date of birth--------------------------------
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 60.0),
                                  child: new Container(
                                    child: new Text(
                                      'Date of Birth',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 0.0, top: 10),
                                  child: new Container(
                                    width: 200,
                                    child: FlatButton(
                                      padding: EdgeInsets.only(
                                          left: 20,
                                          top: 0.0,
                                          right: 0.0,
                                          bottom: 0.0),
                                      onPressed: () {
                                        DatePicker.showDatePicker(context,
                                            showTitleActions: true,
                                            minTime: DateTime(1950, 1, 1),
                                            maxTime: DateTime(2025, 12, 31),
                                            onChanged: (date) {},
                                            onConfirm: (date) {
                                          UserRegistrationPageUtils
                                              .dateFormatter(date);
                                          setState(() {
                                            dateObject = date;
                                            displayedDate =
                                                UserRegistrationPageUtils
                                                    .dateFormatter(date);
                                          });
                                        },
                                            currentTime: dateObject,
                                            locale: LocaleType.en);
                                      },
                                      child: Container(
                                        width: 200,
                                        height: 48,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Container(
                                                width: 200,
                                                height: 48,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          4.0),
                                                  border: Border.all(
                                                    color:
                                                        AppColors.form_border,
                                                    width: 1.2,
                                                    style: BorderStyle.solid,
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    displayedDate,
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontFamily: 'Rajdhani'),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

//user type-----------------------------------

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'User Type',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 0.0, bottom: 20.0),
                    child: Container(
                      width: 300,
                      height: 48,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: 50,
                              height: 50,
                              child: Center(
                                child: Icon(Icons.expand_more,
                                    size: 24, color: Colors.black),
                              ),
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: DropdownButtonHideUnderline(
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: new DropdownButton(
                                  isExpanded: true,
                                  iconSize: 0,
                                  value: _currentUsertype,
                                  items: _userTypedropDownMenuItems,
                                  onChanged: usertypeDropDownItem,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Rajdhani'),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

//Save button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: disabledBtn,
                          onPressed: () {
                            var errorStatus = TextBoxValidation.isPasswordMatch(
                                _passwordFieldController.text,
                                _confirmPasswordFieldController.text);

                            setState(() {
                              _validateConfirmPassword = errorStatus['state'];
                              messageConfirmPassword =
                                  errorStatus['errorMessage'];
                            });

                            if (_nameFieldController.text != "" &&
                                _usernameFieldController.text != "" &&
                                _passwordFieldController.text != "" &&
                                _confirmPasswordFieldController.text != "" &&
                                _validatePassword == true &&
                                _validateConfirmPassword == true &&
                                _validateName == true &&
                                _validateUserNme == true) {
                              checkUserNmae(_usernameFieldController.text);
                            } else {
                              checkValidation();
                            }
                          },
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: disabledBtnFont,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "Add New User",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () async => Future.value(false),
      ),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            if (response['response_data'].toString() != "null") {
              if (response["calledMethod"] == "userData") {
                if (response['response_data'].length == 0) {
                  saveValues();
                } else {
                  showDialog(
                      context: context,
                      builder: (context) => CommonPopupOneButton(
                          context,
                          "error",
                          "There dublicate of User Name.",
                          [],
                          _clearText));
                }
              } else if (response["calledMethod"] == "userDataInsert") {
                Navigator.pushReplacementNamed(context, "/MainPage");
              } else {}
            }
          }
          break;
        }
      default:
        {
          //Do nothing
        }
    }
  }
}
